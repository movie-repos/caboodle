from app import db
from app.models import User
from werkzeug.security import (
    generate_password_hash,
)

confirm = input("Are you sure you want to reset the database? ")

if confirm.lower() != "yes" and confirm.lower() != "y":
    print("Cancelled database reset.")
    exit()

# Delete and then create all the tables
db.drop_all()
db.create_all()

# Create the dummy account
db.session.add(
    User(
        email="NONE",
        password="NONE",
        first_name="NONE",
        last_name="NONE",
    )
)

# Create the admin account
db.session.add(
    User(
        email="admin@admin.com",
        password=generate_password_hash("admin"),
        first_name="Nate",
        last_name="Wood",
        permission=User.ADMIN,
    )
)

# Commit the changes to the DB
db.session.commit()

print("Successfully reset the database.")
