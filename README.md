# Caboodle

This is the web app version of Caboodle.
It automatically pulls metadata for local movie files and allows you
to stream them to other devices.

# Setup

To begin, you need to clone the repository.
Assuming you are using `ssh`, you should do the following:

```sh
$ git clone git@gitlab.com:nightsprol/caboodle.git
$ cd caboodle
```

## Dependencies

Next you need to install a couple dependencies.
The backend is written in Python3 and the frontend is written in HTML and Jinja.
We are using the Flask framework for the server, so you must install it.
There are also some other Flask-related libraries that you must install.
Lastly, we are also using the OMDb API for fetching movie metadata.

```sh
$ pip3 install -r requirements.txt
```

## Running

Before running the server for the first time, you will need to initialize the database one time only.

```sh
$ python3 reset_db.py
```

After that you can simply run the server:

```sh
$ python3 caboodle.py <filepath_to_movies> [--debug]
```

Entering the above command will start up the server.

There are two different ways to run the server, one for development and the other for production.
If you want to actually host a server for others to connect to,
follow the instructions in `./caboodle.py` for switching to production.
However, if you are developing you should always switch back to be more secure.

## Accessing the Website

To access the website, navigate to `localhost:5000` in your browser.
Currently it has only been tested on Google Chrome.

# Contributing

You can contribute by cloning the master branch and creating your own separate development branch.
Then you can submit a merge request which will be reviewed by someone on the primary dev team.

## Directory Layout

If you want to make changes, they should all be done in `./app/`.

- To add server endpoints, modify `./app/views.py`.
- To create webpages, add or modify files in `./app/templates/`.
- To create data objects, add or modify files in `./app/models/`.

Eventually an `./app/tests/` directory will be made for test suites.

# Authors

- Nate Wood [@nightsprol](https://gitlab.com/nightsprol/)