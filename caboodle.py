#!flask/bin/python
from app import app
from argparse import ArgumentParser


def parse_args():
    parser = ArgumentParser()

    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        help="Run a local debug build of the web app.",
    )

    return parser.parse_args()


if __name__ == "__main__":
    # Parse command line args.
    args = parse_args()

    # Start the webserver based on args.
    if args.debug:
        app.run(
            debug=True,
        )
    else:
        app.run(
            host="0.0.0.0",
            threaded=True,
        )
