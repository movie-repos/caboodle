// POST on select field changed

function post_on_select(select_field, endpoint) {
    select_field.on('change', function () {
        $.post(endpoint, {
            value: select_field.value
        });
    });
};