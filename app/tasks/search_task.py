from app.models import Movie


class SearchTask:
    @classmethod
    def run(cls, query):
        terms = query.split(", ")
        movies = Movie.query.filter_by(is_valid=True).all()
        results = []

        for movie in movies:
            for term in terms:
                _terms = term.split(" and ")
                valid = True

                for _term in _terms:
                    if not movie.contains_phrase(_term):
                        valid = False
                        break

                if valid:
                    results.append(movie)

        return results

    @classmethod
    def _strip_whitespace(cls, string):
        return " ".join(string.split())
