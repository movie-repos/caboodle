import omdb
import threading
from os import (
    listdir,
    path,
)
from requests.exceptions import ReadTimeout

from app.models import Movie


class ScanTask:
    @classmethod
    def run(cls, movies_filepath, fetch_errors=False):

        try:
            thread = threading.Thread(
                target=cls._run, args=(movies_filepath, fetch_errors)
            )
            thread.start()
        except Exception:
            print("Error occurred with the [ScanTask] thread.")

    @classmethod
    def _run(cls, movies_filepath, fetch_errors):
        movie_files = listdir(movies_filepath)

        # Logging
        print(
            "-------------------------------------------------------------------------------"
        )
        print("[ScanTask] Beginning scanning...")
        print("[ScanTask] Found %d movies in total" % (len(movie_files)))
        print(
            "-------------------------------------------------------------------------------"
        )

        # Stats tracking
        old_errors, new_errors = 0, 0
        new_movies, new_fetches, total_fetches = 0, 0, 0

        # Run through all the movie files
        for movie_name in movie_files:
            movie_fname, movie_ext = path.splitext(movie_name)

            movie = Movie.query.filter_by(
                filepath=movies_filepath, filename=movie_name
            ).first()

            if movie:
                if not movie.is_valid and not fetch_errors:
                    old_errors += 1
                    continue
                if movie.is_valid:
                    continue

            # Fix the filename
            fixed_name = cls.fix_filename(movie_fname)
            title, year = Movie.split_year(fixed_name)

            fixed_name += movie_ext

            # Get the movie and its metadata
            if not movie:
                movie = Movie(
                    title=title,
                    year=year,
                    filepath=movies_filepath,
                    filename=movie_name,
                )
            else:
                movie.title = title
                movie.year = year

            print(movie.title, f"({movie.year})")

            found, count = cls.search_for_movie(movie)

            if not found:
                print('Failed to find metadata for "%s"' % (movie.title))
                new_errors += 1
            else:
                print(
                    'Found metadata for "%s" after %d fetch(es)' % (movie.title, count)
                )
                new_movies += 1
                new_fetches += count

            total_fetches += count

            # Add the data to the database
            movie.write()

        _avg_fetch_count = (new_fetches * 1.0 / new_movies) if new_movies else 0

        # Print stats
        print(
            "-------------------------------------------------------------------------------"
        )
        print(
            "[ScanTask] Added metadata for %d new movies with %d fetches (%.2f per movie)"
            % (new_movies, new_fetches, _avg_fetch_count)
        )
        print("[ScanTask] Performed a total of %d fetches" % (total_fetches))
        print(
            "[ScanTask] Encountered %d new errors and %d existing errors"
            % (new_errors, old_errors)
        )
        print(
            "-------------------------------------------------------------------------------"
        )

    @classmethod
    def search_for_movie(cls, movie):
        """
        1) Search
        2) Remove standalone numbers; search full title if name changed
        3) Remove series; search episode if name changed
        4) Remove episode; search series if series
        5) Remove year; search full title
        6) Remove year; search just episode
        """

        _fetches = 1

        # 1) Search
        if cls.search(movie):
            return True, _fetches

        # Tokenize into series and episode, removing episode number
        temp = movie.title.split(" ")
        title = ""

        series, episode = "", ""

        for token in temp:
            if not title:
                title = token
            elif not token.isdigit():
                title += " " + token
            elif not series:
                series = movie.title[: len(title)]
                _i = len(title) + len(token) + 2

                if _i < len(movie.title):
                    episode = movie.title[_i:]

        # 2) Remove standalone numbers; search if name changed
        if not movie.title == title:
            _fetches += 1
            if cls.search(movie, title=title):
                return True, _fetches

        # 3) Remove series; search if name changed
        if series and episode:
            _fetches += 1
            if cls.search(movie, title=episode):
                return True, _fetches

        # 4) Remove episode; search if series
        if series:
            _fetches += 1
            if cls.search(movie, title=series):
                return True, _fetches

        # Remove year
        _year = movie.year
        movie.year = None

        # 5) Remove year; search full title
        _fetches += 1
        if cls.search(movie):
            return True, _fetches

        # 6) Remove year; search episode
        if episode:
            _fetches += 1
            if cls.search(movie, title=episode):
                return True, _fetches

        # Fix year and return false
        movie.year = _year
        return False, _fetches

    @classmethod
    def search(cls, movie, title=None, year=None):
        _title = title or movie.title
        _year = year or movie.year
        response = False

        print('Searching for title="%s", year="%s"' % (_title, _year))

        try:
            if _year:
                response = omdb.get(title=_title, year=_year, fullplot=True, timeout=10)
            else:
                response = omdb.get(title=_title, fullplot=True, timeout=10)
        except ReadTimeout as e:
            print(
                'Metadata fetch timed out for "%s". Error: %s'
                % (movie.filename, e.strerror)
            )

        if response:
            movie.from_dict(response)
            movie.is_valid = True
            return True

        movie.is_valid = False
        return False

    @classmethod
    def fix_filename(cls, filename):
        output = ""
        had_space = False

        for c in filename:
            if c == "&":
                output += "and"
                had_space = False
            elif c == " " or c == ".":
                if had_space:
                    continue
                output += " "
                had_space = True
            else:
                output += c
                had_space = False
        return output
