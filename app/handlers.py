import os
from flask import (
    abort,
    flash,
    redirect,
    request,
    url_for,
)
from werkzeug.security import (
    check_password_hash,
    generate_password_hash,
)

from app.models import (
    Movie,
    User,
)
from app import (
    app,
    db,
)
from app.data.session import Session
from app.tasks.scan_task import ScanTask


# Helper to handle a post from the base page
def handle_post():
    if request.form.get("search", None):
        return handle_search()

    if request.form.get("login", None):
        return handle_login()

    if request.form.get("register", None):
        return handle_register()

    # Nothing found
    return redirect(request.url)


# Helper to handle a search from any page
def handle_search():
    return redirect(
        url_for(
            "search",
            query=request.form["search"],
            sort_by="score",
            sort_order="1",
        )
    )


# Login the user
def handle_login():

    # Get the parameters
    email = request.form.get("email", None)
    password = request.form.get("password", None)

    # Error checking
    if not email or not password:
        flash("No email or password found", "info")
        return abort(404)

    # Get the user
    user = User.query.filter_by(email=email).first()

    if not user:
        flash("No user exists with the email %s" % email, "info")
        return redirect(request.url)

    # Verify the password
    if not check_password_hash(user.password, password):
        flash("Invalid password", "warning")
        return redirect(request.url)

    # Everything looks good, add user to session
    Session.login(user.id)
    flash("Successfully logged in as %s" % user.email, "success")

    return redirect(request.url)


# Register the user
def handle_register():

    # Get the parameters
    email = request.form.get("email", None)
    fname = request.form.get("first_name", None)
    lname = request.form.get("last_name", None)
    password = request.form.get("password", None)
    confirm = request.form.get("confirm_password", None)

    if not email or not fname or not lname or not password or not confirm:
        flash("Please fill out all the fields", "info")
        return redirect(request.url)

    # Verify no user with that email
    if User.query.filter_by(email=email).first():
        flash("A user with that email already exists", "info")
        return redirect(request.url)

    # Verify password
    if password != confirm:
        flash("Passwords did not match", "warning")
        return redirect(request.url)

    # Get the hashed password
    hashed_pw = generate_password_hash(password)

    # Create the user
    user = User(email=email, password=hashed_pw)
    user.first_name = fname
    user.last_name = lname

    # Add user to database
    db.session.add(user)
    db.session.commit()

    Session.login(user.id)
    flash("Account created and logged in successfully", "success")

    return redirect(request.url)


def handle_fetch(fetch_errors=False):
    ScanTask.run(app.config.get("MOVIES_DIR"), fetch_errors=fetch_errors)
    return redirect(request.url)


def handle_rename_file():
    movie = Movie.query.get(int(request.form.get("rename_file")))
    new_name = request.form.get("filename")

    src = os.path.join(movie.filepath, movie.filename)
    dst = os.path.join(movie.filepath, new_name)

    try:
        os.rename(src, dst)
        movie.filename = new_name
        movie.write()
        flash("Successfully renamed the file.", "success")
    except Exception:
        flash("An error occurred while renaming the file.", "danger")

    return redirect(request.url)
