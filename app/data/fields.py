# Movie data subtypes


# TODO: Need to support list of ratings.
# Ratings now look like:
#   [{'source': 'Internet Movie Database', 'value': '8.2/10'},
#    {'source': 'Rotten Tomatoes', 'value': '74%'},
#    {'source': 'Metacritic', 'value': '72/100'}]
class Rating:
    WEIGHTS = {
        "None": 0,
        "N/A": 0,
        "Unknown": 0,
        "G": 1,
        "PG": 2,
        "PG-13": 3,
        "R": 4,
        "NC-17": 5,
        "Unrated": 6,
        "Not Rated": 6,
        "NR": 6,
    }

    def __init__(self, value):
        self.rating = value

    def __str__(self):
        return self.rating

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return Rating.WEIGHTS[self.rating] < Rating.WEIGHTS[other.rating]


class Runtime:
    def __init__(self, value):
        self.runtime = -1

        if "min" in value:
            self.runtime = int(value.split()[0])

    def __str__(self):
        if self.runtime < 0:
            return "N/A"

        hours = int(self.runtime / 60)
        minutes = self.runtime % 60

        if hours > 0:
            return "{} hr {} min".format(hours, minutes)
        else:
            return "{} min".format(minutes)

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return self.runtime < other.runtime


class Dollar:
    def __init__(self, value):
        self.amount = -1

        if "$" in value:
            _amount = value.replace(",", "").replace("$", "")
            self.amount = int(_amount)

    def __str__(self):
        if self.amount < 0:
            return "N/A"

        return "${:,}".format(self.amount)

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return self.amount < other.amount


class Metascore:
    def __init__(self, value):
        self.score = -1

        if value.isdigit():
            self.score = int(value)

    def __str__(self):
        if self.score < 0:
            return "N/A"
        return str(self.score)

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return self.score < other.score
