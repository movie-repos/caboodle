from app.models import Movie
from flask_wtf import FlaskForm
from wtforms import (
    IntegerField,
    PasswordField,
    SelectField,
    StringField,
    SubmitField,
)
from wtforms.fields.html5 import DateField
from wtforms.validators import (
    DataRequired,
    Email,
    EqualTo,
    Length,
)


class SearchSortForm(FlaskForm):
    sort_choices = SelectField("Sort by", choices=Movie.SORT_CHOICES)
    sort = SubmitField("Sort")


class AccountDataForm(FlaskForm):
    firstName = StringField("First Name", validators=[DataRequired()])
    lastName = StringField("Last Name", validators=[DataRequired()])
    streetAddress = StringField("Street Address", validators=[DataRequired()])
    city = StringField("City", validators=[DataRequired()])
    state = StringField("State", validators=[DataRequired()])
    zipCode = StringField("Zip Code", validators=[DataRequired()])
    creditCard = StringField("Credit Card", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])
    accountNo = IntegerField("Account Number")


class RegisterForm(AccountDataForm):
    password = PasswordField(
        "Password",
        validators=[
            DataRequired(),
            EqualTo("confirm", message="Passwords must match"),
            Length(min=8, max=24),
        ],
    )
    confirm = PasswordField("Confirm Password", validators=[DataRequired()])
    register = SubmitField("Register")


class RoundTripSearchForm(FlaskForm):
    origin = SelectField(
        "Origin", choices=[("null", "null")], validators=[DataRequired()]
    )
    numPass = IntegerField("Number of Passengers", validators=[DataRequired()])
    returning = DateField("Returning", validators=[DataRequired()])
    search = SubmitField("Search")
