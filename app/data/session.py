from app.models import User
from flask import (
    redirect,
    request,
    session,
)
from functools import wraps


class Session:
    """
    Session Keys
    """

    K_USER_ID = "user"
    K_TEMP = "temp_data"

    """
        Accessors
    """

    @classmethod
    def user(cls):
        return User.query.get(session.get(cls.K_USER_ID, 0))

    @classmethod
    def logged_in(cls):
        return True if session.get(cls.K_USER_ID, None) is not None else False

    @classmethod
    def is_admin(cls):
        return session.user().get("permission", User.USER) == User.ADMIN

    """
        Mutators
    """

    @classmethod
    def temp(cls, data=None):
        if data:
            session[cls.K_TEMP] = data
        return session.get(cls.K_TEMP, {})

    @classmethod
    def login(cls, id):
        session[cls.K_USER_ID] = id

    @classmethod
    def logout(cls):
        session.pop(cls.K_USER_ID)

    """
        Decorators
    """

    @classmethod
    def require_login(cls):
        def middle(func):
            @wraps(func)
            def inner(*args, **kwargs):
                if not cls.logged_in():
                    return redirect(request.referrer)
                return func(*args, **kwargs)

            return inner

        return middle

    @classmethod
    def require_logged_out(cls, dest):
        def middle(func):
            @wraps(func)
            def inner(*args, **kwargs):
                if cls.logged_in():
                    return redirect(dest)
                return func(*args, **kwargs)

            return inner

        return middle

    @classmethod
    def require_admin(cls, dest):
        def middle(func):
            @wraps(func)
            def inner(*args, **kwargs):
                if not cls.is_admin():
                    return redirect(dest)
                return func(*args, **kwargs)

            return inner

        return middle
