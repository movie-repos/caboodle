from datetime import datetime
from flask import url_for

from app import app
from app import db
from app.data import fields
from app.util.math import isfloat

MIN_STRING = 128
MAX_STRING = 1028


class Movie(db.Model):
    # Constants

    DUMMY_FILENAME = "DUMMY (2018).txt"
    DUMMY_FILEPATH = "/dev/null"
    SEARCH_EXCLUDE = ["plot"]

    # ((key, reverse), label)
    SORT_CHOICES = [
        ("score:1", "Score (high to low)"),
        ("score:0", "Score (low to high)"),
        ("title:0", "Title (A-Z)"),
        ("title:1", "Title (Z-A)"),
        ("year:1", "Year (newer)"),
        ("year:0", "Year (older)"),
        ("runtime:1", "Runtime (high to low)"),
        ("runtime:0", "Runtime (low to high)"),
        ("rating:1", "Film Rating (high to low)"),
        ("rating:0", "Film Rating (low to high)"),
    ]

    NUMBER_TYPES = [
        "metascore",
        "imdb_votes",
        "imdb_rating",
        "year",
    ]

    FIELD_TYPES = {
        "box_office": fields.Dollar,
        "runtime": fields.Runtime,
        # TODO: "rating" was changed to a list of "ratings"
        "rating": fields.Rating,
    }

    SKIPPED_FIELDS = ["ratings"]

    # Database Columns

    id = db.Column(db.Integer, primary_key=True)
    imdb_id = db.Column(db.String(12), unique=True)

    title = db.Column(db.String(MAX_STRING), index=True, nullable=False)
    filename = db.Column(db.String(MAX_STRING), index=True, nullable=False)
    filepath = db.Column(db.String(MAX_STRING), index=True, nullable=False)

    year = db.Column(db.Integer, index=True)

    awards = db.Column(db.String(MAX_STRING))
    box_office = db.Column(db.PickleType, index=True)
    country = db.Column(db.String(MIN_STRING))
    dvd = db.Column(db.String(MIN_STRING))
    actors = db.Column(db.String(MAX_STRING), index=True)
    writers = db.Column(db.String(MAX_STRING), index=True)
    directors = db.Column(db.String(MAX_STRING), index=True)
    genre = db.Column(db.String(MAX_STRING), index=True)
    language = db.Column(db.String(MAX_STRING))
    metascore = db.Column(db.Integer, index=True)
    plot = db.Column(db.Text)
    poster = db.Column(db.String(MAX_STRING))
    production = db.Column(db.String(MIN_STRING))
    rating = db.Column(db.PickleType, index=True)
    released = db.Column(db.DateTime, index=True)
    runtime = db.Column(db.PickleType, index=True)
    imdb_rating = db.Column(db.Float, index=True)
    imdb_votes = db.Column(db.Integer, index=True)
    website = db.Column(db.String(MAX_STRING))
    score = db.Column(db.Integer, index=True)

    is_valid = db.Column(db.Boolean)

    # Methods

    def from_dict(self, d):
        # Rename fields
        if "rated" in d:
            d["rating"] = d.pop("rated")
        if "writer" in d:
            d["writers"] = d.pop("writer")
        if "director" in d:
            d["directors"] = d.pop("director")

        if "released" in d and type(d["released"]) is str:
            if d["released"] == "N/A":
                d["released"] = None
            else:
                d["released"] = datetime.strptime(d["released"], "%d %b %Y")

        if "poster" in d and d["poster"] == "N/A":
            with app.app_context():
                d["poster"] = url_for("static", filename="images/404.png")

        # Construct Movie model from dict
        for key, value in d.items():
            if key in self.SKIPPED_FIELDS:
                continue

            _value = value

            if value is None:
                _value = value
            elif key in Movie.FIELD_TYPES:
                _value = Movie.FIELD_TYPES[key](value)
            elif value == "N/A" and key in Movie.NUMBER_TYPES:
                _value = 0
            elif type(value) is datetime:
                _value = value
            elif value.isdigit():
                _value = int(value)
            elif isfloat(value):
                _value = float(value)

            setattr(self, key, _value)

        # Set score field
        self.score = int((self.imdb_rating * 10 + self.metascore) / 2)

    def contains_phrase(self, phrase):
        for key, value in self.__dict__.items():
            if key in Movie.SEARCH_EXCLUDE:
                continue

            if phrase.lower() in str(value).lower():
                return True
        return False

    def write(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    def delete(self):
        if self.id:
            db.session.delete(self)
            db.session.commit()

    # Class methods

    @classmethod
    def read(cls, filepath, dir):
        return cls.query.filter_by(filepath=filepath).first()

    @classmethod
    def split_year(cls, filename):
        if len(filename) < 4:
            return filename, 0

        for i in range(len(filename) - 4, 0, -1):
            if filename[i : i + 4].isdigit():
                return filename[: i - 2], int(filename[i : i + 4])
        return filename, 0


class User(db.Model):

    # Permissions
    USER = 0
    ADMIN = 1

    # Database Columns

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(MIN_STRING), index=True, unique=True, nullable=False)
    password = db.Column(db.String(MIN_STRING), index=True, nullable=False)

    first_name = db.Column(db.String(MIN_STRING), nullable=False)
    last_name = db.Column(db.String(MIN_STRING), nullable=False)

    permission = db.Column(db.Integer, default=USER)
    rating_filter = db.Column(db.Integer)

    # Relationships
    playlists = db.relationship("Playlist", backref="user", lazy=True)


# Relationship table for many-to-many playlists to movies
movies_list = db.Table(
    "movies_list",
    db.Column("movie_id", db.Integer, db.ForeignKey("movie.id"), primary_key=True),
    db.Column(
        "playlist_id", db.Integer, db.ForeignKey("playlist.id"), primary_key=True
    ),
)


class Playlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(MIN_STRING), index=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)

    # Relationships
    movies_list = db.relationship(
        "Movie",
        secondary=movies_list,
        lazy="subquery",
        backref=db.backref("playlists", lazy=True),
    )
