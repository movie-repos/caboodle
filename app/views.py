from os import path
from urllib import parse
from flask import (
    flash,
    redirect,
    render_template,
    request,
    url_for,
)

from app.models import (
    Movie,
)

from app import (
    app,
)
from app.data.forms import SearchSortForm
from app.data.session import Session
import app.handlers as handlers
from app.tasks.search_task import SearchTask
from app.util.xplat import open_file


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        return handlers.handle_post()

    # Load the metadata that exists
    movies = Movie.query.filter_by(is_valid=True).all()

    # Sort the movies by title
    movies.sort(key=lambda x: x.title)

    return render_template(
        "index.html",
        title="Home",
        user=Session.user(),
        movies=movies,
    )


@app.route("/about", methods=["GET", "POST"])
def about():
    movie, _ = get_movie(request)

    if request.method == "POST":
        if request.form.get("play_movie", "False") == "True":
            print(path.join(movie.filepath, movie.filename))
            open_file(path.join(movie.filepath, movie.filename))
        else:
            return handlers.handle_post()

    return render_template(
        "about.html",
        title=movie.title,
        user=Session.user(),
        movie=movie,
    )


@app.route("/watch", methods=["GET", "POST"])
def watch():
    if request.method == "POST":
        return handlers.handle_post()

    movie, file = get_movie(request)

    return render_template(
        "watch.html",
        title="Watch",
        user=Session.user(),
        movie=movie,
        file=file,
    )


@app.route("/logout", methods=["GET", "POST"])
def logout():
    Session.logout()
    flash("Successfully logged out", "success")
    return redirect(url_for("index"))


@Session.require_admin("/")
@app.route("/admin", methods=["GET", "POST"])
def admin():
    if request.method == "POST":
        if request.form.get("fetch", "") == "True":
            return handlers.handle_fetch()
        if request.form.get("fetch_errors", "") == "True":
            return handlers.handle_fetch(fetch_errors=True)
        if request.form.get("rename_file", None):
            return handlers.handle_rename_file()

        return handlers.handle_post()

    # Fetch the errors
    errors = Movie.query.filter_by(is_valid=False).all()
    errors_len = len(errors)

    # Fetch the movies that may be errors
    movies = Movie.query.filter_by(is_valid=True).all()
    potentials = []

    for movie in movies:
        nulls = 0

        for value in movie.__dict__.values():
            if value == "N/A":
                nulls += 1

        if nulls > 2:
            potentials.append(movie)

    potentials_len = len(potentials)

    return render_template(
        "admin.html",
        title="Admin",
        user=Session.user(),
        errors=errors,
        errors_len=errors_len,
        potentials=potentials,
        potentials_len=potentials_len,
    )


@app.route("/search", methods=["GET", "POST"])
def search():
    query = parse.unquote(request.args.get("query", ""))

    sort_by = request.args.get("sort_by", "score")
    sort_order = int(request.args.get("sort_order", 1))

    sort_form = SearchSortForm()

    if request.method == "POST":
        if sort_form.sort.data:
            sort_choice = sort_form.sort_choices.data.split(":")

            sort_by = sort_choice[0]
            sort_order = int(sort_choice[1])

            return redirect(
                url_for("search", query=query, sort_by=sort_by, sort_order=sort_order)
            )
        else:
            return handlers.handle_post()

    movies = SearchTask.run(query)

    sort_form.sort_choices.data = str(sort_by) + ":" + str(sort_order)

    # Sort the movies by title
    movies.sort(key=lambda x: getattr(x, sort_by), reverse=sort_order)

    search = {
        "query": query if query else "ALL",
        "count": len(movies),
    }

    return render_template(
        "search.html",
        title="Search Results",
        user=Session.user(),
        movies=movies,
        search=search,
        form=sort_form,
    )


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html", user=Session.user()), 404


# Helper to get a movie from the request
def get_movie(request):
    id = parse.unquote(request.args.get("movie"))
    movie = Movie.query.get(id)
    file = url_for("movies.static", filename=movie.filename)

    return movie, file
