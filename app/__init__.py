from flask import (
    Blueprint,
    Flask,
)
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import omdb

from config import Config

app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

movies_blueprint = Blueprint(
    "movies",
    __name__,
    static_url_path="/static/movies",
    static_folder=app.config["MOVIES_DIR"],
)
app.register_blueprint(movies_blueprint)

omdb.set_default("apikey", app.config["OMDB_KEY"])

from app import models  # noqa
from app import views  # noqa
