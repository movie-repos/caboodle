import os

_BASEDIR = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.urandom(32)
    OMDB_KEY = "385605ab"

    SESSION_TYPE = "filesystem"
    SERVER_NAME = "127.0.0.1:5000"

    IMAGES_DIR = os.path.join(_BASEDIR, "static", "images")

    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "DATABASE_URL"
    ) or "sqlite:///" + os.path.join(_BASEDIR, "app.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    MOVIES_DIR = "/mnt/d/1_Movies/"
